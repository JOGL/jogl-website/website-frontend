const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  purge: ["./components/**/*.js", "./pages/**/*.js"],
  theme: {
    extend: {
      colors: {
        primary: "#2887CC",
        secondary: "#474747",
      },
      container: {
        center: true,
        padding: {
          DEFAULT: "1rem",
          md: "2rem",
        },
      },
      gridTemplateColumns: {
        footer: "1.4fr 0.9fr 0.7fr 1fr",
      },
      boxShadow: {
        custom: "0px 4px 18px rgba(0, 0, 0, 0.09)",
        custom2:
          "0px 4px 64px rgba(0, 0, 0, 0.09), inset 63.2667px -63.2667px 63.2667px rgba(190, 190, 190, 0.07), inset -63.2667px 63.2667px 63.2667px rgba(255, 255, 255, 0.07)",
      },
    },
    screens: {
      xs: "475px",
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/forms"),
  ],
};
