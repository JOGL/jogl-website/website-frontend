import { getPageData, fetchAPI, getGlobalData } from "utils/api";
import Sections from "@/components/sections";
import Seo from "@/components/elements/seo";
import { useRouter } from "next/router";
import Layout from "@/components/layout";
import ButtonLink from "@/components/elements/button-link";
import { getButtonAppearance } from "utils/button";

// The file is called [[...slug]].js because we're using Next's
// optional catch all routes feature. See the related docs:
// https://nextjs.org/docs/routing/dynamic-routes#optional-catch-all-routes

const DynamicPage = ({ sections, metadata, preview, global, pageContext }) => {
  const router = useRouter();

  // Check if the required data was provided
  if (!router.isFallback && !sections?.length) {
    // show 404 page
    const btn = {
      id: 99,
      newTab: false,
      text: "Go back to homepage",
      type: "primary",
      url: "/",
    };
    return (
      <Layout global={global}>
        <div className="flex items-center px-5 py-16 overflow-hidden relative justify-center lg:p-20">
          <div className="flex-1 rounded-3xl bg-white shadow-xl p-20 text-gray-800 relative items-center text-center max-w-4xl md:(flex text-left)">
            <div className="mb-10 text-gray-600 font-light md:mb-14">
              <h1 className="font-black text-2xl text-primary mb-10 lg:text-5xl">
                Error 404
                <br />
                That page doesn’t exist!
              </h1>
            </div>
            <div className="mb-20 md:mb-0 max-w-sm m-auto">
              <ButtonLink
                button={btn}
                appearance={getButtonAppearance(btn.type, "light")}
                compact
              />
            </div>
          </div>
        </div>
      </Layout>
    );
  }

  // Loading screen (only possible in preview mode)
  if (router.isFallback) {
    return <div className="container">Loading...</div>;
  }

  return (
    <Layout global={global} pageContext={pageContext}>
      {/* Add meta tags for SEO*/}
      <Seo metadata={metadata} />
      {/* Display content sections */}
      <Sections sections={sections} preview={preview} />
    </Layout>
  );
};

export async function getStaticPaths(context) {
  // Get all pages from Strapi
  const allPages = context.locales.map(async (locale) => {
    const localePages = await fetchAPI(`/pages?_locale=${locale}`);
    return localePages;
  });

  const pages = await (await Promise.all(allPages)).flat();

  const paths = pages.map((page) => {
    // Decompose the slug that was saved in Strapi
    const slugArray = !page.slug ? false : page.slug.split("/");

    return {
      params: { slug: slugArray },
      // Specify the locale to render
      locale: page.locale,
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const { params, locale, locales, defaultLocale, preview = null } = context;

  const globalLocale = await getGlobalData(locale);

  // Fetch pages. Include drafts if preview mode is on
  const pageData = await getPageData(
    { slug: !params.slug ? [""] : params.slug },
    locale,
    preview
  );

  if (pageData == null) {
    // Giving the page no props will trigger a 404 page
    return { props: {} };
  }

  // We have the required page data, pass it to the page component
  const { contentSections, metadata, localizations } = pageData;

  return {
    props: {
      preview,
      sections: contentSections,
      metadata,
      global: globalLocale,
      pageContext: {
        slug: pageData.slug,
        locale: pageData.locale,
        locales,
        defaultLocale,
        localizations,
      },
    },
  };
}

export default DynamicPage;
