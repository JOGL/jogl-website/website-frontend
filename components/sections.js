import { useRouter } from "next/router";
import Hero from "@/components/sections/hero";
import HeroFull from "@/components/sections/hero-full";
import HeroFullThree from "@/components/sections/hero-full-three";
import LargeVideo from "@/components/sections/large-video";
import FeatureColumnsGroup from "@/components/sections/feature-columns-group";
import FeatureRowsGroup from "@/components/sections/feature-rows-group";
import BottomActions from "@/components/sections/bottom-actions";
import TestimonialsGroup from "@/components/sections/testimonials-group";
import TeamSection from "@/components/sections/team";
import RichText from "./sections/rich-text";
import Pricing from "./sections/pricing";
import LeadForm from "./sections/lead-form";
import HomeUnlockSciCommunity from "./sections/home-unlock-sci-community.js";
import Partners from "./sections/partners.js";
import PressCoverage from "./sections/press-coverage.js";
import Opportunities from "./sections/opportunities.js";
import Services from "./sections/services.js";
import ServicesMembership from "./sections/services-membership.js";
import ServicesFellowship from "./sections/services-fellowship.js";
import ServicesCustom from "./sections/services-custom.js";
import Legal from "./sections/legal-page.js";

// Map Strapi sections to section components
const sectionComponents = {
  "sections.hero": Hero,
  "sections.hero-full": HeroFull,
  "sections.hero-full-three": HeroFullThree,
  "sections.large-video": LargeVideo,
  "sections.feature-columns-group": FeatureColumnsGroup,
  "sections.feature-rows-group": FeatureRowsGroup,
  "sections.bottom-actions": BottomActions,
  "sections.testimonials-group": TestimonialsGroup,
  "sections.rich-text": RichText,
  "sections.pricing": Pricing,
  "sections.lead-form": LeadForm,
  "sections.team": TeamSection,
  "sections.home-unlock-sci-community": HomeUnlockSciCommunity,
  "sections.parters": Partners,
  "sections.press-coverage": PressCoverage,
  "sections.opportunities": Opportunities,
  "sections.services": Services,
  "sections.services-membership": ServicesMembership,
  "sections.services-fellowship": ServicesFellowship,
  "sections.services-custom": ServicesCustom,
  "sections.legal-page": Legal,
};

// Display a section individually
const Section = ({ sectionData }) => {
  // Prepare the component
  const SectionComponent = sectionComponents[sectionData.__component];

  if (!SectionComponent) {
    return null;
  }

  // Display the section
  return <SectionComponent data={sectionData} />;
};

const PreviewModeBanner = () => {
  const router = useRouter();
  const exitURL = `/api/exit-preview?redirect=${encodeURIComponent(
    router.asPath
  )}`;

  return (
    <div className="py-4 font-semibold tracking-wide text-red-100 uppercase bg-red-600">
      <div className="container">
        Preview mode is on.{" "}
        <a
          className="underline"
          href={`/api/exit-preview?redirect=${router.asPath}`}
        >
          Turn off
        </a>
      </div>
    </div>
  );
};

// Display the list of sections
const Sections = ({ sections, preview }) => {
  return (
    <div className="flex flex-col">
      {/* Show a banner if preview mode is on */}
      {preview && <PreviewModeBanner />}
      {/* Show the actual sections */}
      {sections.map((section) => (
        <Section
          sectionData={section}
          key={`${section.__component}${section.id}`}
        />
      ))}
    </div>
  );
};

export default Sections;
