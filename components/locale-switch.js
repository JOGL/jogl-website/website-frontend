import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import Link from "next/link";
import { WorldIcon } from "./icons/icons";
import { useOnClickOutside } from "../utils/hooks";
import {
  listLocalizedPaths,
  getLocalizedPage,
  localizePath,
} from "../utils/localize";
import { parseCookies } from "utils/parse-cookies";
import Cookies from "js-cookie";
import { MdExpandMore } from "react-icons/md";

const LocaleSwitch = ({ pageContext }) => {
  const isMounted = useRef(false);
  const select = useRef();
  const router = useRouter();
  const [locale, setLocale] = useState();
  const [showing, setShowing] = useState(false);
  const [localizedPaths, setLocalizedPaths] = useState();

  const handleLocaleChange = async (selectedLocale) => {
    // Persist the user's language preference
    // https://nextjs.org/docs/advanced-features/i18n-routing#leveraging-the-next_locale-cookie
    Cookies.set("NEXT_LOCALE", selectedLocale);
    setLocale(selectedLocale);
  };

  useOnClickOutside(select, () => setShowing(false));

  useEffect(() => {
    const changeLocale = async () => {
      if (
        !isMounted.current &&
        cookies.NEXT_LOCALE &&
        cookies.NEXT_LOCALE !== pageContext.locale
      ) {
        // Redirect to locale page if locale mismatch
        const localePage = await getLocalizedPage(
          cookies.NEXT_LOCALE,
          pageContext
        );

        router.push(`${localizePath({ ...pageContext, ...localePage })}`, {
          locale: localePage.locale,
        });
      }

      setShowing(false);
      const localizations = await listLocalizedPaths(pageContext);
      setLocalizedPaths(localizations);
    };

    const cookies = parseCookies();
    setLocale(cookies.NEXT_LOCALE || router.locale);
    changeLocale();

    return () => {
      isMounted.current = true;
    };
  }, [locale, router]);

  return (
    <div ref={select} className="relative ml-3 lg:ml-4 ">
      <button
        className="hover:bg-primary-50 hover:text-primary focus:bg-primary-50 focus:text-primary focus:outline-none flex items-center justify-between px-2 py-1 cursor-pointer h-full rounded-md w-20"
        onClick={() => setShowing(!showing)}
      >
        <WorldIcon />
        <span className="capitalize text-[16px] sm:text-[15px]">{locale}</span>
        <MdExpandMore className="ml-1 text-primary" />
      </button>
      <div
        className={`w-full bg-white p-1 mt-1 border-black border rounded-md ${
          showing ? "absolute" : "hidden"
        }`}
      >
        {localizedPaths &&
          localizedPaths
            // filter to hide current locale from the dropdown
            .filter((lang) => lang.locale !== locale)
            .map(({ href, locale }) => {
              return (
                <Link href={href} key={locale} locale={locale} role="option">
                  <p
                    onClick={() => handleLocaleChange(locale)}
                    className="capitalize hover:bg-primary-50 cursor-pointer p-2 rounded-md text-center hover:text-primary"
                    tabIndex="0"
                  >
                    {locale}
                  </p>
                </Link>
              );
            })}
      </div>
    </div>
  );
};

LocaleSwitch.propTypes = {
  initialLocale: PropTypes.string,
};

export default LocaleSwitch;
