import Image from "../elements/image";
import Markdown from "react-markdown";
import Carousel from "../elements/carousel";

const PressCoverage = ({ data }) => {
  return (
    <section className="container pt-12 pb-16 text-lg">
      {/* <p className="ml-2 text-2xl font-semibold tracking-wide text-gray-400 uppercase"> */}
      <p className="inline-flex items-start ml-2 text-3xl font-bold tracking-wide">
        {/* {data.label} */}
        <img src="/Press.png" className="w-6 mr-2" />
        {data.label}
      </p>
      <div className="mb-8 swiper mySwiper3">
        <div className="py-2 pl-2 swiper-wrapper">
          {data.pressCoverageCards.map((pressCoverage, i) => (
            <div
              className="relative flex flex-wrap content-between h-auto px-10 pt-10 pb-5 bg-white swiper-slide rounded-3xl shadow-custom"
              key={i}
            >
              <div>
                <div className="flex justify-between mb-5">
                  <h2 className="font-semibold text-2xl line-clamp-2 max-h-[65px]">
                    {pressCoverage.title}
                  </h2>
                  <div className="inline-block ml-3">
                    <Image
                      media={pressCoverage.logo}
                      className="w-[100px] max-w-[100px] inline-block"
                    />
                  </div>
                </div>
                {pressCoverage.subtitle && (
                  <p className="pt-2 pb-6 text-2xl font-medium text-gray-600">
                    {pressCoverage.subtitle}
                  </p>
                )}
                <Markdown
                  source={pressCoverage.excerpt}
                  className="text-gray-700 line-clamp-4"
                />
              </div>
              <div className="w-full pt-2">
                <p className="text-right hover:opacity-80">
                  <a href={pressCoverage.link} target="_blank">
                    {data.learnMoreText}
                  </a>
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className="right-0 swiper-button-next"></div>
        <div className="left-0 swiper-button-prev"></div>
      </div>
      <h2 className="mt-10 mb-5 text-3xl font-semibold tracking-wide text-center text-gray-400 uppercase">
        {data.otherCoveragesText}
      </h2>
      <Carousel data={data.otherCoveragesCarousel} />
    </section>
  );
};

export default PressCoverage;
