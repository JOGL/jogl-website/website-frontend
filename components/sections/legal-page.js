import Markdown from "react-markdown";

const Legal = ({ data }) => {
  return (
    <section className="container px-4 pb-16 m-auto md:pt-12 max-w-prose md:px-0">
      <h2 className="mb-16 title">{data.title}</h2>
      <div className="prose prose-lg rich-text">
        <Markdown source={data.content} />
      </div>
    </section>
  );
};

export default Legal;
