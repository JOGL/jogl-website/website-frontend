import classNames from "classnames";
import { useState } from "react";
import Image from "../elements/image";
import CustomLink from "../elements/custom-link";

const TestimonialsGroup = ({ data }) => {
  // Only show one testimonial at a time
  const [selectedTestimonialIndex, setSelectedTestimonialIndex] = useState(0);
  const selectedTestimonial = data.testimonials[selectedTestimonialIndex];

  return (
    <section className="pt-12 pb-16 text-lg text-center bg-gray-200">
      <h2 className="mb-4 title">{data.title}</h2>
      <p className="mb-4 text-gray-700">{data.description}</p>
      <CustomLink link={data.link}>
        <span className="text-blue-700 with-arrow hover:underline">
          {data.link.text}
        </span>
      </CustomLink>
      {/* Current testimonial card */}
      <div className="flex flex-col w-8/12 max-w-5xl mx-auto mt-10 text-left bg-white shadow-md sm:w-8/12 sm:shadow-xl sm:flex-row">
        <Image
          media={selectedTestimonial.picture}
          className="flex-shrink-0 object-cover w-full md:w-4/12"
        />
        <div className="flex flex-col justify-between px-4 py-4 sm:px-12 sm:pt-12 sm:pb-4">
          <div>
            <Image
              media={selectedTestimonial.logo}
              className="w-auto h-8 mt-2 mb-6 sm:mb-10 sm:mt-0"
            />
            <p className="mb-6 italic">"{selectedTestimonial.text}"</p>
            <p className="text-base font-bold sm:text-sm">
              {selectedTestimonial.authorName}
            </p>
            <p className="text-base sm:text-sm">
              {selectedTestimonial.authorTitle}
            </p>
          </div>
          <CustomLink
            link={{
              url: selectedTestimonial.link,
              text: "",
              newTab: false,
              id: 0,
            }}
          >
            <span className="mt-6 tracking-wide text-blue-700 uppercase hover:underline with-arrow sm:self-end sm:mt-0">
              Read story
            </span>
          </CustomLink>
        </div>
      </div>
      {/* Change selected testimonial (only if there is more than one) */}
      {data.testimonials.length > 1 && (
        <div className="flex flex-row justify-center gap-4 mt-10">
          {data.testimonials.map((testimonial, index) => (
            <button
              onClick={() => setSelectedTestimonialIndex(index)}
              className={classNames(
                // Common classes
                "rounded-full h-3 w-3",
                {
                  "bg-gray-500": index !== selectedTestimonialIndex,
                  "bg-primary": index === selectedTestimonialIndex,
                }
              )}
              key={testimonial.id}
            ></button>
          ))}
        </div>
      )}
      {/* Logos list */}
      <div className="flex flex-row flex-wrap items-center justify-center gap-6 px-6 mt-10 sm:gap-20 sm:px-0">
        {data.logos.map((logo) => (
          <Image
            media={logo.logo}
            className="object-contain w-auto h-8 max-w-xs"
            key={logo.id}
          />
        ))}
      </div>
    </section>
  );
};

export default TestimonialsGroup;
