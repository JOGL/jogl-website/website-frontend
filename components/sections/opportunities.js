import { getButtonAppearance } from "utils/button";
import ButtonLink from "../elements/button-link";
import { MdPlace } from "react-icons/md";
import Markdown from "react-markdown";
import Image from "../elements/image";

const Opportunities = ({ data }) => {
  return (
    <section className="pb-16 text-lg md:pt-12">
      {/* Header */}
      <div className="container">
        <h2 className="mb-2 title lg:text-6xl">{data.title}</h2>
        <p className="py-5 text-gray-700 sm:py-10 max-w-prose">
          {data.description}
        </p>
      </div>
      {/* Job offers */}
      <div className="container mt-4">
        <p className="mb-5 font-semibold tracking-wide text-gray-400 uppercase">
          {data.jobOffersTitle}
        </p>
        <div className="bg-[#F8FAFD] md:p-10 rounded-md">
          {data.jobPositions.length !== 0 && (
            <div className="mb-2 bg-white shadow sm:rounded-md md:mb-6">
              <ul className="divide-y divide-gray-200">
                {data.jobPositions.map((position, i) => {
                  const url = position.link || position.file?.url;
                  return (
                    <li key={i}>
                      <a
                        href={url}
                        target="_blank"
                        className="block hover:bg-gray-50"
                      >
                        <div className="px-4 py-4 sm:px-6">
                          <div className="flex flex-col justify-between xs:items-center xs:flex-row">
                            <p className="font-medium truncate text-md text-primary">
                              {position.title}
                            </p>
                            <div className="flex flex-shrink-0 xs:ml-2">
                              <p className="inline-flex px-2 text-sm font-semibold leading-5 text-green-800 bg-green-100 rounded-full">
                                {position.workTime}
                              </p>
                            </div>
                          </div>
                          <div className="mt-2 sm:flex sm:justify-between">
                            <div className="sm:flex">
                              <p className="flex items-center mt-2 text-gray-500 text-md sm:mt-0">
                                <MdPlace className="w-5 h-5" />
                                {position.location}
                              </p>
                            </div>
                            <div className="flex items-center mt-2 text-gray-500 text-md sm:mt-0">
                              <p>Closing on {position.closingDate}</p>
                            </div>
                          </div>
                        </div>
                      </a>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
          <div className="p-6 md:p-0">
            <Markdown source={data.jobOffersSpontApplicationText} />
          </div>
        </div>
      </div>
      {/* Contribute */}
      <div className="flex flex-wrap justify-center gap-10 mt-20">
        <div className="bg-white rounded-3xl max-w-[25rem] shadow-custom flex flex-wrap p-10 justify-center text-center">
          <Image
            media={data.contributeIcon}
            className="w-[150px] object-contain"
          />
          <p className="py-8">{data.contributeText}</p>
          <ButtonLink
            button={data.contributeButton}
            appearance={getButtonAppearance(
              data.contributeButton.type,
              "light"
            )}
          />
        </div>
        {/* Volunteer */}
        <div className="bg-white rounded-3xl max-w-[25rem] shadow-custom flex flex-wrap p-10 justify-center text-center">
          <Image
            media={data.volunteerIcon}
            className="w-[150px] object-contain "
          />
          <p className="py-8">{data.volunteerText}</p>
          <ButtonLink
            button={data.volunteerButton}
            appearance={getButtonAppearance(
              data.contributeButton.type,
              "light"
            )}
          />
        </div>
        {/* Ambassador */}
        <div className="bg-white rounded-3xl max-w-[25rem] shadow-custom flex flex-wrap p-10 justify-center text-center">
          <Image
            media={data.ambassadorIcon}
            className="w-[120px] object-contain "
          />
          <p className="py-8">{data.ambassadorText}</p>
          <div className="grid justify-center gap-2">
            {data.ambassadorButton.map((button) => (
              <ButtonLink
                button={button}
                appearance={getButtonAppearance(button.type, "light")}
                key={button.id}
              />
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Opportunities;
