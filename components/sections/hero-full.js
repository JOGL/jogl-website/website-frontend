import Markdown from "react-markdown";
import ButtonLink from "../elements/button-link";
import Image from "../elements/image";
import { getButtonAppearance } from "utils/button";
import TestimonialCarousel from "../elements/testimonial-carousel";

const HeroFull = ({ data }) => {
  return (
    <>
      <section className={data.reverse ? " bg-[#F8FAFD]" : "bg-white"}>
        <div className="container">
          <div
            className={`flex flex-col items-center justify-between pt-20 pb-10 ${
              data.reverse ? "lg:flex-row-reverse" : "lg:flex-row"
            }`}
          >
            {/* Left column for content */}
            <div className="flex-1 sm:pr-16">
              {/* Hero section label */}
              <p className="font-semibold tracking-wide text-gray-300 uppercase">
                {data.label}
              </p>
              {/* Title and desc */}
              <h1 className="mt-2 mb-4 title-small sm:mt-0 sm:mb-2">
                {data.title}
              </h1>
              <p className="mt-6 mb-12 text-xl text-justify text-gray-500 sm:mt-8 sm:text-left">
                {data.description}
              </p>
              {/* Features list */}
              {!data.reverse && (
                <div className="grid grid-cols-2 gap-4">
                  {data.listFeatures?.map((feature, i) => (
                    <div
                      key={i}
                      className="inline-flex items-center self-center"
                    >
                      <Image media={feature.icon} className="w-5 mr-2" />
                      <p>{feature.title}</p>
                    </div>
                  ))}
                </div>
              )}
              {/* CTA Button */}
              <div className="flex flex-row mt-12">
                <ButtonLink
                  button={data.buttons}
                  appearance={getButtonAppearance(data.buttons.type, "light")}
                  key={data.buttons.id}
                />
              </div>
              {/* Small rich text */}
              <div className="mt-4 text-base md:text-sm sm:mt-3 rich-text-hero">
                <Markdown source={data.smallTextWithLink} />
              </div>
            </div>
            {/* Right column for the image */}
            <Image
              media={data.picture}
              className={`flex-shrink-0 object-contain w-full sm:w-8/12 lg:w-7/12 mt-12 lg:mt-0 ${
                data.reverse ? "lg:mr-20" : ""
              }`}
            />
          </div>
        </div>
        {/* Testimonial carousel (optional) */}
        {data.testimonialsCarousel.length !== 0 && (
          <TestimonialCarousel
            carouselData={data.testimonialsCarousel}
            isRerverse={data.reverse}
          />
        )}
      </section>
    </>
  );
};

export default HeroFull;
