import { getButtonAppearance } from "utils/button";
import ButtonLink from "../elements/button-link";
import Image from "../elements/image";

const ServicesMembership = ({ data }) => {
  return (
    <section className="relative pb-16 text-lg md:pt-12">
      {/* Header */}
      <div className="container">
        <h2 className="mb-2 title">{data.title}</h2>
        <p className="py-5 whitespace-pre-wrap text-secondary sm:py-10 max-w-prose">
          {data.description}
        </p>
      </div>
      {/* Content */}
      <div
        className="container flex flex-col items-center justify-center w-full mt-10"
        // style={{
        //   backgroundImage:
        //     "url('https://images.unsplash.com/photo-1519681393784-d120267933ba?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1124&q=100')",
        // }}
      >
        {/* Info box */}
        <div className="p-6 md:p-10 bg-white max-w-[760px] flex flex-col mx-auto my-10 bg-opacity-60 backdrop-filter backdrop-blur-lg shadow-custom2 rounded-xl text-xl">
          <h3 className="text-2xl font-bold">{data.infoTitle}</h3>
          <div className="pt-10 pb-10 space-y-4">
            {data.listText.map((t) => (
              <div className="flex" key={t.text}>
                <span className="relative flex items-center justify-center w-3 h-3 p-3 mr-5 text-gray-400 bg-white rounded-full shadow-lg text-md">
                  <i className="fa-solid fa-plus"></i>
                </span>
                {t.text}
              </div>
            ))}
          </div>
          <div className="text-center">
            <p className="mb-10 italic">{data.infoDetails}</p>
            <div className="w-[fit-content] mx-auto">
              <ButtonLink
                button={data.button}
                appearance={getButtonAppearance(data.button.type, "light")}
                key={data.button.id}
              />
            </div>
          </div>
        </div>
        {/* Current members carousel */}
        <div className="flex flex-col p-10 mx-auto mt-20 bg-white bg-opacity-60 backdrop-filter backdrop-blur-lg shadow-custom2 rounded-xl">
          <h3 className="text-3xl font-black text-center">
            {data.featuredObjectsLabel}
          </h3>
          <div className="grid grid-cols-1 gap-4 mt-8 md:grid-cols-3">
            {data.featuredObjects.map((object, i) => (
              <div
                className="relative h-auto overflow-hidden bg-white shadow-custom rounded-xl"
                key={i}
              >
                <div className="flex items-center hover:opacity-90">
                  <a href={object.link} target="_blank" className="w-full">
                    <Image
                      media={object.banner}
                      className="w-full h-[150px] object-cover"
                    />
                  </a>
                </div>
                {/* <span className="absolute inline-block px-3 py-1 mb-2 mr-2 text-sm font-semibold text-gray-700 capitalize rounded-full bg-gray-50/60 top-3 right-3 backdrop-blur">
                    {object.type}
                  </span> */}
                <div className="px-6 py-4">
                  <div className="text-3xl font-semibold text-secondary hover:underline">
                    <a href={object.link} target="_blank">
                      {object.title}
                    </a>
                  </div>
                  <div className="mb-2 text-gray-400 capitalize">
                    {object.type} - {object.subtitle}
                  </div>
                  <p className="my-3 text-base text-gray-600 line-clamp-5">
                    {object.description}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>

      <img
        src="/membership-shape2.svg"
        className="absolute bottom-[600px] left-0 z-[-1]"
      />
      <img
        src="/membership-shape.svg"
        className="absolute bottom-[-100px] right-0 z-[-1]"
      />
    </section>
  );
};

export default ServicesMembership;
