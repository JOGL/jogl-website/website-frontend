import Carousel from "../elements/carousel";

const Partners = ({ data }) => {
  return (
    <section className="text-lg py-24 bg-[#F8FAFD]">
      <h2 className="mb-5 text-3xl font-semibold tracking-wide text-center text-gray-400 uppercase">
        {data.corePartnersTitle}
      </h2>
      <Carousel data={data.corePartnersCarousel} fixed />
      <h2 className="mb-5 text-3xl font-semibold tracking-wide text-center text-gray-400 uppercase mt-14">
        {data.secondaryPartnersTitle}
      </h2>
      <Carousel data={data.otherPartnersCarousel} />
    </section>
  );
};

export default Partners;
