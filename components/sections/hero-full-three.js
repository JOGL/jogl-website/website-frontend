import { useState } from "react";
import Markdown from "react-markdown";
import { getButtonAppearance } from "utils/button";
import ButtonLink from "../elements/button-link";
import Image from "../elements/image";

const HeroFullThree = ({ data }) => {
  const [activeBox, setActiveBox] = useState(1);
  // transform data object to show only data from 1 of the 3 blocs (depending on activeBox state)
  const profileDatas = Object.keys(data)
    .filter((key) => key.includes(activeBox))
    .reduce((obj, key) => {
      return Object.assign(obj, {
        [key.slice(0, -1)]: data[key],
      });
    }, {});
  return (
    <section className="relative">
      <div className="container mt-20">
        {/* --- Choose profile --- */}
        <h4 className="pb-10 text-4xl font-black text-center">{data.title}</h4>
        <div className="flex flex-wrap content-center justify-center gap-5 md:gap-10">
          <div
            className={`${
              activeBox === 1 ? "bg-gray-100" : "bg-white"
            } w-36 sm:w-72 lg:w-[31%] p-4 md:p-8 text-center text-gray-600 self-stretch rounded-md shadow-md cursor-pointer hover:bg-gray-100`}
            tabIndex="0"
            as="button"
            onClick={() => setActiveBox(1)}
            onKeyPress={() => setActiveBox(1)}
          >
            <h2 className="text-xl font-bold md:text-2xl">
              {data.profileBtnTitle1}
            </h2>
            <p className="mt-2 italic text-md md:text-xl md:mt-0">
              {data.profileBtnDesc1}
            </p>
          </div>
          <div
            className={`${
              activeBox === 2 ? "bg-gray-100" : "bg-white"
            } w-36 sm:w-72 lg:w-[31%] p-4 md:p-8 text-center text-gray-600 self-stretch rounded-md shadow-md cursor-pointer hover:bg-gray-100`}
            onClick={() => setActiveBox(2)}
            onKeyPress={() => setActiveBox(2)}
            tabIndex="0"
            as="button"
          >
            <h2 className="text-xl font-bold md:text-2xl">
              {data.profileBtnTitle2}
            </h2>
            <p className="mt-2 italic text-md md:text-xl md:mt-0">
              {data.profileBtnDesc2}
            </p>
          </div>
          <div
            className={`${
              activeBox === 3 ? "bg-gray-100" : "bg-white"
            } w-36 sm:w-72 lg:w-[31%] p-4 md:p-8 text-center text-gray-600 self-stretch rounded-md shadow-md cursor-pointer hover:bg-gray-100`}
            onClick={() => setActiveBox(3)}
            onKeyPress={() => setActiveBox(3)}
            tabIndex="0"
            as="button"
          >
            <h2 className="text-xl font-bold md:text-2xl">
              {data.profileBtnTitle3}
            </h2>
            <p className="mt-2 italic text-md md:text-xl md:mt-0">
              {data.profileBtnDesc3}
            </p>
          </div>
        </div>
        {/* --- Content --- */}
        <div className="flex flex-col items-center justify-between p-8 mt-10 mb-20 bg-white md:p-10 lg:flex-row bg-opacity-60 backdrop-filter backdrop-blur-lg shadow-custom2 rounded-xl">
          {/* Left 4 md:p-column for content */}
          <div className="flex-1 sm:pr-16">
            {/* Hero section label */}
            <p className="font-semibold tracking-wide text-gray-300 uppercase">
              {profileDatas.label}
            </p>
            {/* Title and desc */}
            <h2 className="my-4 text-[2rem] font-bold sm:mb-2">
              {profileDatas.title}
            </h2>
            <p className="mt-6 mb-12 text-[1.35rem] text-justify text-gray-500 sm:mt-8 sm:text-left">
              {profileDatas.description}
            </p>
            {/* Features list */}
            <div className="grid grid-cols-2 gap-4">
              {profileDatas.listFeatures?.map((feature, i) => (
                <div key={i} className="inline-flex items-center self-center">
                  <img src={`/icons/icon${i + 1}${activeBox}.svg`} />
                  <p className="ml-2 text-lg">{feature.title}</p>
                </div>
              ))}
            </div>
            {/* CTA Button */}
            <div className="flex flex-row mt-12">
              <ButtonLink
                button={profileDatas.buttons}
                appearance={getButtonAppearance(
                  profileDatas.buttons.type,
                  "light"
                )}
                key={profileDatas.buttons.id}
              />
            </div>
            {/* Small rich text */}
            <div className="mt-4 text-base md:text-sm sm:mt-3 rich-text-hero">
              <Markdown source={profileDatas.smallTextWithLink} />
            </div>
          </div>
          {/* Right column for the image */}
          <Image
            media={profileDatas.picture}
            className="flex-shrink-0 object-contain w-[520px] mt-10 lg:mt-0"
          />
        </div>
      </div>

      <img
        src="/home-shape2.svg"
        className="absolute bottom-[200px] left-0 z-[-1]"
      />
      <img
        src="/home-shape3.svg"
        className="absolute bottom-[-300px] right-0 z-[-1] hidden lg:block"
      />
    </section>
  );
};

export default HeroFullThree;
