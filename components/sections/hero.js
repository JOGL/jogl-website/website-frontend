import ButtonLink from "../elements/button-link";
import Image from "../elements/image";
import { getButtonAppearance } from "utils/button";
import ReactMarkdown from "react-markdown";

const Hero = ({ data }) => {
  return (
    <section>
      {/* Main */}
      <div className="relative">
        <div className="container flex flex-col items-center justify-between pb-40 lg:flex-row sm:pt-20">
          {/* Left - content */}
          <div className="flex-1 sm:pr-8">
            <h1 className="mt-2 mb-4 title sm:mt-0 sm:mb-2">{data.title}</h1>
            <p className="my-12 text-2xl text-justify text-gray-500 sm:text-left">
              {data.description}
            </p>
            <div className="flex flex-row flex-wrap space-x-3 xs:space-x-4">
              {data.buttons.map((button) => (
                <ButtonLink
                  button={button}
                  appearance={getButtonAppearance(button.type, "light")}
                  key={button.id}
                />
              ))}
            </div>
          </div>
          {/* Right - Image */}
          <Image
            media={data.picture}
            className="flex-shrink-0 object-contain w-full mt-12 lg:w-6/12 xl:w-7/12 lg:mt-0"
          />
          <img
            src="/home-shape.svg"
            className="absolute top-0 right-0 z-[-1] opacity-80 hidden md:block"
          />
        </div>
      </div>

      {/* They built community */}
      <div className="container flex flex-col gap-4 mb-12 lg:flex-row">
        {/* Left part */}
        <div className="w-full lg:w-3/5">
          <h2 className="mt-2 mb-4 text-4xl font-black leading-snug sm:mt-0 sm:mb-2">
            {data.featuredObjectsLabel}
          </h2>
          <div className="mt-10 text-xl">
            <ReactMarkdown source={data.featuredObjectsDesc} />
          </div>
        </div>
        {/* Right part */}

        <div className="hidden lg:flex w-1/2 lg:w-2/5 h-[fit-content] px-4 py-2 ml-4 lg:self-center rounded shadow-lg bg-gray-50">
          <div className="mr-3">
            <Image media={data.testimonialImg} className="w-40" />
          </div>
          <div>
            {data.testimonialText}
            <div className="inline-flex justify-end w-full">
              {data.testimonialName} -&nbsp;
              <ReactMarkdown source={data.testimonialLink} />
            </div>
          </div>
        </div>
      </div>
      {/* Carousel */}
      <div className="container swiper mySwiper pb-7">
        <div className="py-2 pl-2 mt-2 swiper-wrapper">
          {data.featuredObjects.map((object, i) => (
            <div
              className="relative h-auto overflow-hidden bg-white swiper-slide shadow-custom rounded-xl"
              key={i}
            >
              <div className="flex items-center hover:opacity-90">
                <a href={object.link} target="_blank" className="w-full">
                  <Image
                    media={object.banner}
                    className="w-full h-[150px] object-cover"
                  />
                </a>
              </div>
              <span className="absolute inline-block px-3 py-1 mb-2 mr-2 text-sm font-semibold text-gray-700 capitalize rounded-full bg-gray-50/60 top-3 right-3 backdrop-blur">
                {object.link.includes("peer-review")
                  ? "Call for proposals"
                  : object.type}
              </span>
              <div className="px-6 py-4">
                <div className="mb-2 text-3xl font-semibold text-gray-800 hover:underline">
                  <a href={object.link} target="_blank">
                    {object.title}
                  </a>
                </div>
                {object.subtitle && (
                  <p className="pt-2 text-2xl font-medium text-gray-600">
                    {object.subtitle}
                  </p>
                )}
                <p className="my-3 text-base text-gray-600 line-clamp-5">
                  {object.description}
                </p>
              </div>
            </div>
          ))}
        </div>
        <div className="right-0 swiper-button-next"></div>
        <div className="left-0 swiper-button-prev"></div>
      </div>
    </section>
  );
};

export default Hero;
