import Markdown from "react-markdown";
import ButtonLink from "../elements/button-link";
import Image from "../elements/image";
import { getButtonAppearance } from "utils/button";

const HomeUnlockSciCommunity = ({ data }) => {
  return (
    <>
      {/* Text and features cards */}
      <section className="container mx-auto text-center pt-14" id="learnMore">
        <h2 className="text-4xl font-bold mb-10">{data.title}</h2>
        <div className="text-xl max-w-prose m-auto mb-20 md:mb-24 rich-text text-justify sm:text-center text-gray-500">
          <Markdown source={data.description} />
        </div>
        <h2 className="text-3xl font-bold">{data.featureTitle}</h2>
        <div className="flex items-center justify-center mt-20">
          <div className="grid grid-cols-1 gap-14 sm:gap-16 lg:gap-8 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 xl:grid-cols-4 text-center">
            {data.listFeatures.map((feature, i) => (
              <div
                className={`
                  relative bg-white py-6 px-4 rounded-3xl h-40 md:h-48 lg:h-56 shadow-custom ${
                    // we add margin-top to card 2 and 3, on desktop only
                    i === 1 || i === 2 ? "lg:mt-20" : ""
                  }`}
                key={i}
              >
                <div className="text-white flex items-center justify-center -mt-14 lg:-mt-16">
                  <Image
                    media={feature.icon}
                    className="h-[50px] md:h-[60px] lg:h-[80px]"
                  />
                </div>
                <div className="mt-8">
                  <div className="text-xl my-2 text-gray-600">
                    <Markdown source={feature.text} />
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>

      {/* Cta banner */}
      <div className="bg-[#F8FAFD] mt-14">
        <div className="mx-auto py-12 px-4 sm:px-6 lg:py-16 lg:px-8 lg:flex lg:items-center lg:justify-between container">
          <h2 className="text-xl tracking-tight md:text-2xl text-[#6E82AE]">
            <span className="block font-semibold">{data.ctaBanner.title}</span>
            <span className="block">{data.ctaBanner.description}</span>
          </h2>
          <div className="mt-8 flex lg:mt-0 lg:flex-shrink-0">
            <ButtonLink
              button={data.ctaBanner.button}
              appearance={getButtonAppearance(
                data.ctaBanner.button.type,
                "light"
              )}
              key={data.ctaBanner.button.id}
            />
          </div>
        </div>
      </div>

      <section className="container mx-auto"></section>
    </>
  );
};

export default HomeUnlockSciCommunity;
