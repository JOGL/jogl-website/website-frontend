import { getButtonAppearance } from "utils/button";
import ButtonLink from "../elements/button-link";
import Image from "../elements/image";
import Markdown from "react-markdown";
import { Dialog, Transition, Switch } from "@headlessui/react";
import { Fragment, useState } from "react";
import { Tabs, TabList, Tab, TabPanels, TabPanel } from "@reach/tabs";
import { MdClose, MdLink, MdArrowForward, MdArrowBack } from "react-icons/md";
import "@reach/tabs/styles.css";

const TeamSection = ({ data }) => {
  const [open, setOpen] = useState(false);
  const [modalMemberIndex, setModalMemberIndex] = useState(0);
  const [tabIndex, setTabIndex] = useState(0);
  const [enabled, setEnabled] = useState(true);
  const membersNumber =
    tabIndex === 0
      ? data.TeamMemberCore.length
      : data.TeamMemberAmbassadors.length;
  const modalMemberData =
    tabIndex === 0 ? data.TeamMemberCore : data.TeamMemberAmbassadors;
  const socialLinkIcon = (type) => {
    switch (type) {
      case "twitter":
        return <i className="fab fa-twitter" aria-hidden="true" />;
      case "instagram":
        return <i className="fab fa-instagram" aria-hidden="true" />;
      case "twitter":
        return <i className="fab fa-twitter" aria-hidden="true" />;
      case "gitlab":
        return <i className="fab fa-gitlab" aria-hidden="true" />;
      case "medium":
        return <i className="fab fa-medium" aria-hidden="true" />;
      case "youtube":
        return <i className="fab fa-youtube" aria-hidden="true" />;
      case "linkedin":
        return <i className="fab fa-linkedin" aria-hidden="true" />;
      case "jogl":
        return (
          <img
            src="/logo-b-w.png"
            className="w-6 h-6 opacity-70 hover:opacity-100"
          />
        );
      case "website":
        return <MdLink className="w-5 h-[22px]" />;
      default:
        return <MdLink className="w-5 h-[22px]" />;
    }
  };
  return (
    <section className="container pb-16 text-lg md:pt-12">
      {/* Header */}
      <div className="text-center">
        <h2 className="mb-4 title lg:text-6xl">{data.title}</h2>
        <p className="pt-5 pb-10 m-auto text-gray-700 sm:pt-10 max-w-prose">
          {data.description}
        </p>
        <div className="flex flex-row flex-wrap justify-center">
          <ButtonLink
            button={data.button}
            appearance={getButtonAppearance(data.button.type, "light")}
          />
        </div>
      </div>
      {/* Tabs */}
      <Tabs
        className="mt-12 lg:mt-20"
        index={tabIndex}
        onChange={(index) => setTabIndex(index)}
      >
        <TabList className="!bg-white tabs flex-wrap justify-center">
          <Tab className="!m-2 xs:w-1/3 xs:!m-0">
            {data.TeamMemberCoreTitle}
          </Tab>
          <Tab className="!m-2 xs:w-1/3 xs:!m-0">
            {data.TeamMemberAmbassadorsTitle}
          </Tab>
          <Tab className="!m-2 xs:w-1/3 xs:!m-0">
            {data.TeamMemberCollaboratorsTitle}
          </Tab>
        </TabList>
        <TabPanels>
          {/* Core Members list */}
          <TabPanel>
            <div className="py-10 mx-auto max-w-7xl lg:py-24">
              <ul className="space-y-4 sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 lg:grid-cols-3 lg:gap-8">
                {data.TeamMemberCore.length > 0 &&
                  data.TeamMemberCore.map((person, index) => (
                    <li
                      key={person.full_name}
                      className="px-6 py-10 bg-white rounded-lg shadow-custom"
                    >
                      <div className="space-y-6 xl:space-y-10">
                        <Image
                          media={person.profile_img}
                          className="w-40 h-40 mx-auto rounded-full xl:w-56 xl:h-56"
                        />
                        <div className="space-y-2">
                          <div className="space-y-1 text-lg font-medium leading-6 text-center">
                            <h3>{person.full_name}</h3>
                            <p className="text-primary">{person.role}</p>
                          </div>
                          <div className="flex items-center justify-center space-x-4 text-center">
                            {person.SocialLinks.length > 0 &&
                              person.SocialLinks.map((link, i) => (
                                <a
                                  key={i}
                                  href={link.url}
                                  target="_blank"
                                  className="text-gray-400 hover:text-gray-500"
                                >
                                  <span className="sr-only">{link.type}</span>
                                  {socialLinkIcon(link.type)}
                                </a>
                              ))}
                          </div>
                          <div className="rich-text-banner py-5 sm:py-3 max-h-[100px] overflow-hidden text-[#6E82AE] leading-8 text-justify rich-text">
                            <Markdown source={person.bio} />
                          </div>
                          <div className="w-full">
                            <div
                              className="text-right text-[#afaaaa] opacity-80 cursor-pointer underline hover:opacity-100"
                              onClick={() => {
                                setOpen(true);
                                setModalMemberIndex(index);
                              }}
                              onKeyPress={() => {
                                setOpen(true);
                                setModalMemberIndex(index);
                              }}
                              as="button"
                              tabIndex="0"
                            >
                              {data.showMoreText}
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                  ))}
              </ul>
            </div>
          </TabPanel>
          {/* Ambassadors Members list */}
          <TabPanel>
            <div className="py-10 mx-auto max-w-7xl lg:py-24">
              <ul className="space-y-4 sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 lg:grid-cols-3 lg:gap-8">
                {data.TeamMemberAmbassadors?.map((person, index) => (
                  <li
                    key={person.full_name}
                    className="px-6 py-8 m-2 bg-white rounded-lg shadow-custom"
                  >
                    <div className="space-y-6 xl:space-y-10">
                      <Image
                        media={person.profile_img}
                        className="w-40 h-40 mx-auto rounded-full xl:w-56 xl:h-56"
                      />
                      <div className="space-y-2">
                        <div className="space-y-1 text-lg font-medium leading-6 text-center">
                          <h3>{person.full_name}</h3>
                          <p className="text-primary">{person.role}</p>
                        </div>
                        <div className="flex justify-center space-x-5 text-center">
                          {person.SocialLinks.length > 0 &&
                            person.SocialLinks.map((link, i) => (
                              <a
                                key={i}
                                href={link.url}
                                className="text-gray-400 hover:text-gray-500"
                              >
                                <span className="sr-only">{link.type}</span>
                                {socialLinkIcon(link.type)}
                              </a>
                            ))}
                        </div>
                        <div className="rich-text-banner py-5 sm:py-3 max-h-[100px] overflow-hidden text-[#6E82AE] leading-8 text-justify rich-text">
                          <Markdown source={person.bio} />
                        </div>
                        <div className="w-full">
                          <div
                            className="text-right text-[#afaaaa] opacity-80 cursor-pointer underline hover:opacity-100"
                            onClick={() => {
                              setOpen(true);
                              setModalMemberIndex(index);
                            }}
                            onKeyPress={() => {
                              setOpen(true);
                              setModalMemberIndex(index);
                            }}
                            as="button"
                            tabIndex="0"
                          >
                            {data.showMoreText}
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </TabPanel>
          {/* Collaborators Members list */}
          <TabPanel>
            <div className="py-10 mx-auto max-w-7xl lg:py-24">
              {/* Switch between present and past collaborators */}
              <div className="flex justify-center mb-5 space-x-3">
                <span>Past</span>
                <Switch
                  checked={enabled}
                  onChange={setEnabled}
                  className={`${
                    enabled ? "bg-primary" : "bg-gray-200"
                  } relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary`}
                >
                  <span
                    aria-hidden="true"
                    className={`${
                      enabled ? "translate-x-5" : "translate-x-0"
                    } pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200`}
                  />
                </Switch>
                <span>Present</span>
              </div>
              <ul className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                {data.TeamMemberCollaborators?.filter(
                  (person) => (enabled ? !person.past : person.past) // filter between past and present collaborators
                ).map((person) => (
                  <li
                    key={person.full_name}
                    className="px-6 py-8 m-2 bg-white rounded-lg shadow-custom"
                  >
                    <div className="space-y-6 xl:space-y-10">
                      <Image
                        media={person.profile_img}
                        className="mx-auto rounded-full xs:w-40 xs:h-40 xl:w-56 xl:h-56"
                      />
                      <div className="space-y-2">
                        <div className="space-y-1 text-lg font-medium leading-6 text-center">
                          <h3>{person.full_name}</h3>
                          <p className="text-primary">{person.role}</p>
                        </div>
                        <div className="flex justify-center space-x-5 text-center">
                          {person.SocialLinks.length > 0 &&
                            person.SocialLinks.map((link, i) => (
                              <a
                                key={i}
                                href={link.url}
                                className="text-gray-400 hover:text-gray-500"
                              >
                                <span className="sr-only">{link.type}</span>
                                {socialLinkIcon(link.type)}
                              </a>
                            ))}
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </TabPanel>
        </TabPanels>
      </Tabs>

      {/* Modal */}
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 z-10 overflow-y-auto"
          open={open}
          onClose={setOpen}
        >
          <div className="flex items-center justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="inline-block w-full max-w-2xl px-16 pt-5 pb-4 overflow-hidden text-left align-middle transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:py-6">
                {/* Close button */}
                <div className="absolute top-2 right-3">
                  <button
                    type="button"
                    className="inline-flex justify-center px-2 py-1 text-2xl font-medium text-gray-600 border border-transparent rounded-full hover:text-black focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-primary"
                    onClick={() => setOpen(false)}
                  >
                    <span className="sr-only">Close</span>
                    <MdClose className="w-auto h-8" />
                  </button>
                </div>
                <div className="mt-3">
                  {/* Content */}
                  <div className="space-y-6 xl:space-y-10">
                    <Image
                      media={modalMemberData[modalMemberIndex]?.profile_img}
                      className="w-64 h-64 mx-auto rounded-full xl:w-56 xl:h-56"
                    />
                    <div className="space-y-2">
                      <div className="space-y-1 text-lg font-medium leading-6 text-center">
                        <h3 className="text-[23px]">
                          {modalMemberData[modalMemberIndex]?.full_name}
                        </h3>
                        <p className="text-primary">
                          {modalMemberData[modalMemberIndex]?.role}
                        </p>
                      </div>
                      <div className="flex items-center justify-center space-x-4 text-center">
                        {modalMemberData[modalMemberIndex]?.SocialLinks.length >
                          0 &&
                          modalMemberData[modalMemberIndex]?.SocialLinks.map(
                            (link, i) => (
                              <a
                                key={i}
                                href={link.url}
                                target="_blank"
                                className="text-gray-400 hover:text-gray-500"
                              >
                                <span className="sr-only">{link.type}</span>
                                {socialLinkIcon(link.type)}
                              </a>
                            )
                          )}
                      </div>
                    </div>
                  </div>

                  <div className="mt-7">
                    <div className="text-lg text-justify rich-text">
                      <Markdown
                        source={modalMemberData[modalMemberIndex]?.bio}
                      />
                    </div>
                  </div>
                  {/* Previous member button (don't show for first user) */}
                  {modalMemberIndex > 0 && (
                    <button
                      onClick={() => setModalMemberIndex(modalMemberIndex - 1)}
                    >
                      <MdArrowBack className="absolute w-auto h-6 text-gray-600 top-1/2 left-3 hover:text-black" />
                    </button>
                  )}
                  {/* Next member button (don't show for last user) */}
                  {modalMemberIndex < membersNumber - 1 && (
                    <button
                      onClick={() => setModalMemberIndex(modalMemberIndex + 1)}
                    >
                      <MdArrowForward className="absolute w-auto h-6 text-gray-600 top-1/2 right-3 hover:text-black" />
                    </button>
                  )}
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
    </section>
  );
};

export default TeamSection;
