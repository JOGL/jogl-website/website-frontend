import { getButtonAppearance } from "utils/button";
import ButtonLink from "../elements/button-link";
import Image from "../elements/image";

const Services = ({ data }) => {
  return (
    <section className="relative pb-16 text-lg md:pt-12">
      {/* Header */}
      <div className="container">
        <h2 className="mb-2 text-center title lg:text-6xl">{data.title}</h2>
        <p className="py-5 mx-auto text-center text-gray-700 sm:py-10 max-w-prose">
          {data.description}
        </p>
        <div className="flex flex-wrap justify-center mt-40 gap-y-52 gap-x-10">
          {/* Fellowship */}
          <div className="bg-white rounded-3xl md:max-w-[25rem] backdrop-filter backdrop-blur-lg shadow-custom2 bg-opacity-60 text-secondary flex flex-col items-center p-10 justify-center">
            <Image
              media={data.fellowShipPicture}
              className="flex-shrink-0 object-contain w-[200px] mt-[-150px]"
            />
            <h2 className="mt-8 text-2xl font-medium">
              {data.fellowshipTitle}
            </h2>
            <p className="py-8">{data.fellowshipText}</p>
            <ButtonLink
              button={data.fellowshipButton}
              appearance={getButtonAppearance(
                data.fellowshipButton.type,
                "light"
              )}
            />
          </div>
          {/* Membership */}
          <div className="bg-white rounded-3xl md:max-w-[25rem] backdrop-filter backdrop-blur-lg shadow-custom2 bg-opacity-60 text-secondary flex flex-col items-center p-10 justify-center">
            <Image
              media={data.membershipPicture}
              className="flex-shrink-0 object-contain w-[200px] mt-[-150px]"
            />
            <h2 className="mt-8 text-2xl font-medium">
              {data.membershipTitle}
            </h2>
            <p className="py-8">{data.membershipText}</p>
            <ButtonLink
              button={data.membershipButton}
              appearance={getButtonAppearance(
                data.membershipButton.type,
                "light"
              )}
            />
          </div>
          {/* Custom */}
          <div className="bg-white rounded-3xl md:max-w-[25rem] backdrop-filter backdrop-blur-lg shadow-custom2 bg-opacity-60 text-secondary flex flex-col items-center p-10 justify-center">
            <Image
              media={data.customPicture}
              className="flex-shrink-0 object-contain w-[130px] mt-[-170px]"
            />
            <h2 className="mt-8 text-2xl font-medium">{data.customTitle}</h2>
            <p className="py-8">{data.customText}</p>
            <ButtonLink
              button={data.customButton}
              appearance={getButtonAppearance(data.customButton.type, "light")}
            />
          </div>
        </div>
      </div>
      <img
        src="/service-shape2.svg"
        className="absolute bottom-[-200px] left-[-70px] z-[-1] hidden sm:block"
      />
      <img
        src="/service-shape.svg"
        className="absolute bottom-[300px] right-0 z-[-1]"
      />
    </section>
  );
};

export default Services;
