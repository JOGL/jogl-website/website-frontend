import Navbar from "./elements/navbar";
import Footer from "./elements/footer";
import NotificationBanner from "./elements/notification-banner";
import { useState } from "react";

const Layout = ({ children, global, pageContext }) => {
  const { navbar, footer, notificationBanner } = global;

  const [bannerIsShown, setBannerIsShown] = useState(
    notificationBanner.shouldShow
  );
  return (
    <div className="flex flex-col justify-between min-h-screen">
      {/* Aligned to the top */}
      <div className="flex-1">
        {notificationBanner && bannerIsShown && (
          <NotificationBanner
            data={notificationBanner}
            closeSelf={() => setBannerIsShown(false)}
          />
        )}
        <Navbar navbar={navbar} pageContext={pageContext} />
        <div className="z-20 w-full bg-[#8C5DBF] p-[1.2rem] text-white">
          <div className="max-w-[1280px] mx-auto text-[14px]">
            🎉 <strong>JOGL is soon launching a new version</strong>. All the
            users of the v1 will be migrated to the new version.{" "}
            <strong>
              In the time being, we do not allow the creation of new users on
              the v1 platform
            </strong>
            .
          </div>
        </div>
        <main className="mt-[4.5rem] sm:mt-14">{children}</main>
      </div>
      {/* Aligned to the bottom */}
      <Footer footer={footer} />
    </div>
  );
};

export default Layout;
