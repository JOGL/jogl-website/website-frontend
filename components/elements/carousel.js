import Image from "../elements/image";

const Carousel = ({ data, fixed }) => {
  if (!fixed)
    return (
      <div className="overflow-hidden whitespace-nowrap slider-container text-center">
        <div className="slider-content-wrapper">
          <div className="inline-block">
            <div className="overflow-hidden whitespace-nowrap company-logos-container text-center">
              {data.map((company, i) => (
                <div className="carousel-item" key={i}>
                  <a href={company.link} target="_blank">
                    <Image
                      media={company.icon}
                      className="object-contain w-28 h-16 max-w-none grayscale hover:grayscale-0"
                    />
                  </a>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  return (
    <div className="mt-6 container">
      <div className="flow-root">
        <div className="flex flex-wrap justify-between">
          {data.map((company, i) => (
            <div
              className="mx-4 my-2 flex flex-grow flex-shrink-0 lg:flex-grow-0 justify-center"
              key={i}
            >
              <a href={company.link} target="_blank">
                <Image
                  media={company.icon}
                  className="w-28 h-16 object-contain opacity-80 hover:opacity-100"
                />
              </a>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Carousel;
