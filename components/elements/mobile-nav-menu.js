import PropTypes from "prop-types";
import { MdClose } from "react-icons/md";
import { BsChevronDown, BsChevronRight } from "react-icons/bs";
import Image from "./image";
import {
  mediaPropTypes,
  linkPropTypes,
  buttonLinkPropTypes,
} from "utils/types";
import ButtonLink from "./button-link";
import { useLockBodyScroll } from "utils/hooks";
import { getButtonAppearance } from "utils/button";
import CustomLink from "./custom-link";
import { useState } from "react";
import { useRouter } from "next/router";

const MobileNavMenu = ({ navbar, closeSelf }) => {
  const router = useRouter();
  const [showService, setShowService] = useState(false);
  const [serviceMenu, setServiceMenu] = useState();
  // Prevent window scroll while mobile nav menu is open
  useLockBodyScroll();

  return (
    <div className="fixed top-0 left-0 z-10 w-screen h-screen pb-6 overflow-y-scroll bg-white">
      <div className="container flex flex-col h-full">
        {/* Top section */}
        <div className="flex flex-row items-center justify-between py-4">
          {/* Company logo */}
          <Image media={navbar.logo} className="object-contain w-auto h-8" />
          {/* Close button */}
          <button onClick={closeSelf} className="px-1 py-1">
            <MdClose className="w-auto h-8" />
          </button>
        </div>
        {/* Bottom section */}
        <div className="flex flex-col w-11/12 mx-auto mt-6">
          <ul className="flex flex-col items-baseline space-y-6 text-xl list-none divide-y divide-gray-200 mb-14">
            {navbar.links.map((navLink) => (
              <li
                key={navLink.id}
                className="block w-full"
                onClick={() => !navLink.dropDownId && closeSelf()}
              >
                {!navLink.dropDownId ? (
                  <CustomLink link={navLink}>
                    <div className="flex flex-row items-center justify-between pt-6 hover:text-gray-900">
                      <span>{navLink.text}</span>
                    </div>
                  </CustomLink>
                ) : (
                  <>
                    <div
                      className="flex flex-row items-center justify-between pt-6 cursor-pointer hover:text-gray-900"
                      onClick={() => {
                        (navLink.dropDownId === serviceMenu || !showService) &&
                          setShowService((prevState) => !prevState);
                        setServiceMenu(navLink.dropDownId);
                      }}
                    >
                      {navLink.text}
                      {navLink.dropDownId === serviceMenu && showService ? (
                        <BsChevronDown className="w-auto h-8" />
                      ) : (
                        <BsChevronRight className="w-auto h-8" />
                      )}
                    </div>
                    {navLink.dropDownId === serviceMenu && showService && (
                      <ul className="flex flex-col items-baseline mt-4 mb-4 ml-4 space-y-4 text-xl list-none divide-y divide-gray-400">
                        {navbar.subLinks
                          .filter(
                            ({ dropDownId }) => dropDownId === serviceMenu
                          )
                          .map((navLink) => (
                            <li
                              key={navLink.id}
                              className="block w-full"
                              onClick={closeSelf}
                            >
                              <CustomLink link={navLink} locale={router.locale}>
                                <div className="items-center justify-between px-2 py-1 pt-6 text-gray-700 hover:text-black">
                                  {navLink.text}
                                </div>
                              </CustomLink>
                            </li>
                          ))}
                      </ul>
                    )}
                  </>
                )}
              </li>
            ))}
          </ul>
          {/* CTA buttons */}
          <div className="flex space-x-3">
            {navbar.button.map((button) => (
              <ButtonLink
                button={button}
                appearance={getButtonAppearance(button.type, "light")}
                compact
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

MobileNavMenu.propTypes = {
  navbar: PropTypes.shape({
    logo: mediaPropTypes,
    links: PropTypes.arrayOf(linkPropTypes),
    button: PropTypes.arrayOf(buttonLinkPropTypes),
  }),
  closeSelf: PropTypes.func,
};

export default MobileNavMenu;
