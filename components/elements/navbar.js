import { useState } from "react";
import PropTypes from "prop-types";
import Link from "next/link";
import { useRouter } from "next/router";

import MobileNavMenu from "./mobile-nav-menu";
import ButtonLink from "./button-link";
import Image from "./image";
import CustomLink from "./custom-link";
import LocaleSwitch from "../locale-switch";

import { getButtonAppearance } from "utils/button";
import {
  mediaPropTypes,
  linkPropTypes,
  buttonLinkPropTypes,
} from "utils/types";

import { MdMenu } from "react-icons/md";

const Navbar = ({ navbar, pageContext }) => {
  const router = useRouter();
  const [mobileMenuIsShown, setMobileMenuIsShown] = useState(false);
  const [showService, setShowService] = useState(false);
  const [serviceMenu, setServiceMenu] = useState();

  return (
    <>
      {/* The actual navbar */}
      <nav className="sticky top-0 z-10 w-full py-4 bg-white border-b-2 border-gray-200 shadow-sm sm:py-2">
        <div className="container flex flex-row items-center justify-between">
          {/* Content aligned to the left */}
          <div className="flex flex-row items-center">
            <Link href="/">
              <a>
                <Image
                  media={navbar.logo}
                  className="object-contain w-auto h-8"
                />
              </a>
            </Link>
            {/* List of links on desktop */}
            <ul className="flex-row items-baseline hidden space-x-3 list-none lg:flex lg:space-x-4 ml-7">
              {navbar.links.map((navLink) => (
                <li key={navLink.id}>
                  {!navLink.dropDownId ? (
                    <CustomLink link={navLink} locale={router.locale}>
                      <div className="px-2 py-1 text-gray-700 hover:text-black">
                        {navLink.text}
                      </div>
                    </CustomLink>
                  ) : (
                    <div
                      className="px-2 py-1 text-gray-700 cursor-pointer hover:text-black"
                      tabIndex="0"
                      onClick={() => {
                        (navLink.dropDownId === serviceMenu || !showService) &&
                          setShowService((prevState) => !prevState);
                        setServiceMenu(navLink.dropDownId);
                      }}
                      onKeyPress={() => {
                        (navLink.dropDownId === serviceMenu || !showService) &&
                          setShowService((prevState) => !prevState);
                        setServiceMenu(navLink.dropDownId);
                      }}
                    >
                      {navLink.text}
                      <span className="ml-2" aria-hidden>
                        ▾
                      </span>
                    </div>
                  )}
                </li>
              ))}
            </ul>
          </div>
          <div className="flex">
            {/* Locale Switch Mobile */}
            {pageContext?.localizations && (
              <div className="self-center lg:hidden">
                <LocaleSwitch pageContext={pageContext} />
              </div>
            )}
            {/* Hamburger menu on mobile */}
            <button
              onClick={() => setMobileMenuIsShown(true)}
              className="block p-1 lg:hidden"
            >
              <MdMenu className="w-auto h-8" />
            </button>
            {/* CTA buttons */}
            <div className="flex space-x-3">
              {navbar.button.map((button, i) => (
                <div className="hidden lg:block" key={i}>
                  <ButtonLink
                    button={button}
                    appearance={getButtonAppearance(button.type, "light")}
                    compact
                  />
                </div>
              ))}
            </div>
            {/* Locale Switch Desktop */}
            {pageContext?.localizations && (
              <div className="hidden lg:block">
                <LocaleSwitch pageContext={pageContext} />
              </div>
            )}
          </div>
        </div>
        <div className={`container mt-5 ${showService ? "block" : "hidden"}`}>
          <ul className="flex-row items-baseline hidden space-x-3 list-none lg:flex lg:space-x-4">
            {navbar.subLinks
              .filter(({ dropDownId }) => dropDownId === serviceMenu)
              .map((navLink) => (
                <li
                  key={navLink.id}
                  // close subnav links section when clicking one of the links
                  onClick={() => setShowService && setShowService(false)}
                >
                  <CustomLink link={navLink} locale={router.locale}>
                    <div className="px-2 py-1 text-gray-700 hover:text-black">
                      {navLink.text}
                    </div>
                  </CustomLink>
                </li>
              ))}
          </ul>
        </div>
      </nav>

      {/* Mobile navigation menu panel */}
      {mobileMenuIsShown && (
        <MobileNavMenu
          navbar={navbar}
          closeSelf={() => setMobileMenuIsShown(false)}
        />
      )}
    </>
  );
};

Navbar.propTypes = {
  navbar: PropTypes.shape({
    logo: PropTypes.shape({
      image: mediaPropTypes,
      url: PropTypes.string,
    }),
    links: PropTypes.arrayOf(linkPropTypes),
    button: PropTypes.arrayOf(buttonLinkPropTypes),
  }),
  initialLocale: PropTypes.string,
};

export default Navbar;
