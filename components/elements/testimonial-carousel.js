import Image from "../elements/image";
import Markdown from "react-markdown";
import { Quote } from "../icons/icons";

const TestimonialCarousel = ({ carouselData, isReverse }) => (
  <div
    className={`mb-8 container swiper mySwiper ${isReverse ? "reverse" : ""}`}
  >
    <div className="swiper-wrapper pl-2 py-2">
      {carouselData.map((testimonial, i) => (
        <div
          className="swiper-slide relative bg-white pt-10 pb-5 px-10 rounded-3xl h-auto shadow-custom flex flex-wrap space-between content-between"
          key={i}
        >
          <div>
            <div className="absolute top-0 left-0 transform translate-x-5 translate-y-5 h-8 w-8 text-gray-600">
              <Quote />
            </div>
            <div className="quote">{testimonial.quote}</div>
          </div>
          <div className="flex items-center mt-5">
            <Image
              media={testimonial.image}
              className="inline-block h-20 w-20 rounded-full mr-5 object-cover"
            />
            <div>
              <p className="font-semibold">{testimonial.authorName}</p>
              <p className="text-[#92A7D8] mt-[-5px]">
                {testimonial.authorRole}
              </p>
              <Markdown
                source={testimonial.object}
                className="text-[#92A7D8] mt-[-5px]"
              />
            </div>
          </div>
        </div>
      ))}
    </div>
    <div className="swiper-button-next right-3"></div>
    <div className="swiper-button-prev left-3"></div>
  </div>
);

export default TestimonialCarousel;
