import Button from "../elements/button";
import { useState } from "react";
import { fetchAPI } from "utils/api";
import * as yup from "yup";
import { Formik, Form, Field } from "formik";

const NewsletterForm = () => {
  const [loading, setLoading] = useState(false);
  const [hasSubscribed, setHasSubscribed] = useState(false);
  const LeadSchema = yup.object().shape({
    email: yup.string().email().required(),
  });

  return (
    <Formik
      initialValues={{ email: "" }}
      validationSchema={LeadSchema}
      onSubmit={async (values, { setSubmitting, setErrors }) => {
        setLoading(true);

        try {
          setErrors({ api: null });
          await fetchAPI("/lead-form-submissions", {
            method: "POST",
            body: JSON.stringify({
              email: values.email,
              location: "Home Page Bottom",
            }),
          });
        } catch (err) {
          setErrors({ api: err.message });
        }
        setHasSubscribed(true);
        setLoading(false);
        setSubmitting(false);
      }}
    >
      {({ errors, touched, isSubmitting }) => (
        <div>
          <Form className="flex flex-col md:flex-row gap-4">
            <Field
              className="text-base focus:outline-none py-4 md:py-0 px-4 border-2 rounded-md"
              type="email"
              name="email"
              placeholder="Enter your email"
            />
            <button
              type="submit"
              className="w-full bg-primary flex items-center justify-center border border-transparent rounded-md py-2 px-4 text-base font-medium text-white hover:bg-primary focus:ring-2 focus:ring-offset-2 focus:ring-primary"
              disabled={isSubmitting}
              // loading={loading}
            >
              Subscribe
            </button>
          </Form>
          <p className="text-red-500 text-sm mt-1 ml-2 text-left">
            {(errors.email && touched.email && errors.email) || errors.api}
          </p>
          <p className="text-green-500 text-sm mt-1 ml-2 text-left">
            {hasSubscribed && "Thanks for subscribing!"}
          </p>
        </div>
      )}
    </Formik>
  );
};

export default NewsletterForm;
