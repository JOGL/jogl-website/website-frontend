import Image from "./image";
import PropTypes from "prop-types";
import { linkPropTypes, mediaPropTypes } from "utils/types";
import CustomLink from "./custom-link";
import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { MdClose } from "react-icons/md";

const Footer = ({ footer }) => {
  const [open, setOpen] = useState(false);
  const showCurrentYear = () => {
    return new Date().getFullYear();
  };
  const social = [
    {
      name: "Linkedin",
      href: "https://www.linkedin.com/company/jogl/about/",
      icon: "fab fa-linkedin",
    },
    {
      name: "Twitter",
      href: "https://twitter.com/justonegiantlab",
      icon: "fab fa-twitter",
    },
    {
      name: "Facebook",
      href: "https://www.facebook.com/justonegiantlab/",
      icon: "fab fa-facebook-f",
    },
    {
      name: "Instagram",
      href: "https://www.instagram.com/justonegiantlab/",
      icon: "fab fa-instagram",
    },
    {
      name: "Medium",
      href: "https://medium.com/justonegiantlab/latest",
      icon: "fab fa-medium",
    },
    {
      name: "Youtube",
      href: "https://www.youtube.com/channel/UCvgRJdZ4OoTyo8tuJNVf4fA/videos/",
      icon: "fab fa-youtube",
    },
    {
      name: "Gitlab",
      href: "https://gitlab.com/JOGL/JOGL",
      icon: "fab fa-gitlab",
    },
  ];

  return (
    <footer className="bg-gray-100" aria-labelledby="footerHeading">
      <h2 id="footerHeading" className="sr-only">
        Footer
      </h2>
      <div className="container">
        <div className="px-4 py-12 mx-auto max-w-7xl sm:px-6 lg:pt-16 lg:pb-7 lg:px-0">
          <div className="xl:grid xl:grid-cols-3 xl:gap-8">
            <div className="mb-16 space-y-8 xl:col-span-1 xl:mb-0">
              {footer.logo && (
                <Image
                  media={footer.logo}
                  className="object-contain w-auto h-16 md:h-24"
                />
              )}
              <p className="text-base text-gray-500">{footer.shortDesc}</p>
              <div className="flex space-x-6 social-links-container">
                {social.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    target="_blank"
                    className="text-2xl text-gray-400 hover:text-gray-500"
                  >
                    <span className="sr-only">{item.name}</span>
                    <i
                      className={item.icon}
                      aria-hidden="true"
                      title={item.name}
                    ></i>
                  </a>
                ))}
              </div>
            </div>
            {/* <nav className="flex flex-row flex-wrap items-start mb-10 lg:gap-20 lg:justify-end"> */}
            <nav className="grid grid-cols-2">
              {footer.columns.map((footerColumn) => (
                <div key={footerColumn.id}>
                  <h3 className="font-semibold tracking-wider text-gray-400 uppercase text-md">
                    {footerColumn.title}
                  </h3>
                  <ul className="mt-4 space-y-4">
                    {footerColumn.links.map((link) => (
                      <li
                        key={link.id}
                        className="px-1 py-1 -mx-1 text-gray-700 hover:text-gray-900"
                      >
                        <CustomLink
                          className="text-base text-gray-500 hover:text-gray-900"
                          link={link}
                        >
                          {link.text}
                        </CustomLink>
                      </li>
                    ))}
                  </ul>
                </div>
              ))}
            </nav>
            <div className="mt-12 xl:mt-0">
              <h3 className="font-semibold tracking-wider text-gray-400 uppercase text-md">
                Newsletter
              </h3>
              <p className="mt-4 text-base text-gray-500">
                {footer.newsletterText}
              </p>
              <div className="mt-3 rounded-md sm:flex-shrink-0">
                <button
                  type="submit"
                  className="flex items-center justify-center px-4 py-2 text-base font-medium text-white border border-transparent rounded-md bg-primary hover:bg-primary/90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary"
                  onClick={() => setOpen(true)}
                >
                  {footer.newsletterButton}
                </button>
              </div>
              {/* <NewsletterForm /> */}
            </div>
          </div>
        </div>
        <div className="py-8 mt-6 border-t border-gray-200">
          <p className="text-base text-center text-gray-400">
            Just One Giant Lab - {showCurrentYear()}
            <br />
            {footer.smallText}
          </p>
          <a
            href="https://vercel.com?utm_source=jogl&utm_campaign=oss"
            target="_blank"
          >
            <img
              src="https://www.datocms-assets.com/31049/1618983297-powered-by-vercel.svg"
              alt="vercel banner"
              className="max-w-[11rem] opacity-70 hover:opacity-80 m-auto mt-4"
            />
          </a>
        </div>
      </div>

      {/* Modal */}
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          static
          className="fixed inset-0 z-10 overflow-y-auto"
          open={open}
          onClose={setOpen}
        >
          <div className="flex items-center justify-center min-h-screen px-4 pt-4 pb-20 text-center sm:block sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
            </Transition.Child>

            <span
              className="hidden sm:inline-block sm:align-middle sm:h-screen"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <div className="inline-block max-w-md px-4 pt-5 pb-4 overflow-hidden text-left align-bottom transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:align-middle sm:max-w-xl sm:w-full sm:p-6">
                {/* Close button */}
                <div className="absolute top-2 right-3">
                  <button
                    type="button"
                    className="inline-flex justify-center px-2 py-1 text-2xl font-medium text-gray-600 border border-transparent rounded-full hover:text-black focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-primary"
                    onClick={() => setOpen(false)}
                  >
                    <span className="sr-only">Close</span>
                    <MdClose className="w-auto h-8" />
                  </button>
                </div>
                <div className="w-[350px] sm:w-[500px]">
                  <iframe
                    src="https://eepurl.com/giW4uf"
                    width="100%"
                    height="500"
                  />
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>
    </footer>
  );
};

Footer.propTypes = {
  footer: PropTypes.shape({
    logo: mediaPropTypes.isRequired,
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
          .isRequired,
        title: PropTypes.string.isRequired,
        links: PropTypes.arrayOf(linkPropTypes),
      })
    ),
    smallText: PropTypes.string.isRequired,
  }),
};

export default Footer;
